package hr.fer.apptownfunk.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {
	@GetMapping("/")
	public String showIndexPage() {
		return "index";
	}
	
	@GetMapping("/audio")
	public String showAudioPage() {
		return "audio"; 
	}

}