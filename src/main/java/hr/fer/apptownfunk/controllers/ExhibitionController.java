package hr.fer.apptownfunk.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import hr.fer.apptownfunk.models.Exhibitions;
import hr.fer.apptownfunk.services.ExhibitionService;
import hr.fer.apptownfunk.services.ExhibitsService;

@Controller
public class ExhibitionController {

	@Autowired
	private ExhibitionService exhibitionService;
	
	@Autowired
	private ExhibitsService exhibitsService; 

	@GetMapping("/exhibitionUpload")
	public String showExhibitionUploadPage() {
		return "exhibitionUpload";
	}

	@PostMapping("/exhibitions")
	public String addNewExhibition(ModelMap map, @RequestParam String name, @RequestParam String description,
			@RequestParam String begin, @RequestParam String end) {
		Date dateBegin = null;
		Date dateEnd = null;
		
		try {
			dateBegin = new SimpleDateFormat("yyyy-MM-dd").parse(begin);
			dateEnd = new SimpleDateFormat("yyyy-MM-dd").parse(end);
		} catch (ParseException e) {
		}
		
		Exhibitions exhibition = new Exhibitions(0, name, description, dateBegin, dateEnd);
		exhibitionService.addExhibition(exhibition);
		return getAllExhibitions(map); 

	}

	@PutMapping("/exhibitions/{id}")
	public void updateExhibition(@RequestBody Exhibitions exhibit) {
		exhibitionService.updateExhibition(exhibit);
	}

	@DeleteMapping("/exhibitions/{id}")
	public void deleteExhibition(@PathVariable Integer id) {
		exhibitionService.deleteExhibition(id);
	}

	@GetMapping("/exhibitions")
	public String getAllExhibitions(ModelMap model) {
		model.addAttribute("exhibitions", exhibitionService.getAllExhibitions());
		return "allExhibitions";
	}

	@GetMapping("/exhibitions/{id}")
	public ModelAndView getExhibitionById(ModelMap model, @PathVariable int id) {
		ModelAndView modelAndView = new ModelAndView("exhibition");
		model.addAttribute("exhibition", exhibitionService.getExhibition(id));
		model.addAttribute("exhibits", exhibitsService.getExhibitsByExhibitionId(id)); 
		
		return modelAndView; 
	}
}
