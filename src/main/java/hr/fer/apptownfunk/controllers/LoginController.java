package hr.fer.apptownfunk.controllers;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hr.fer.apptownfunk.models.Users;
import hr.fer.apptownfunk.services.UserService;
import hr.fer.apptownfunk.services.UserServiceRequestType;

@Controller
public class LoginController {
	
	@Autowired
	UserService userService;
	
	@GetMapping("/login")
	public String showLoginPage() {
		return "login";
	}
	
	@PostMapping("/login")
    public String userValidation(HttpServletRequest request, ModelMap model, @RequestParam String username, @RequestParam String password){
        boolean isValidUser = userService.validateUser(username, password);

        if (!isValidUser) {
            model.put("errorMessage", "Username or password is not valid");
            
            return "login";
        }

        Users user = userService.getUser(username, UserServiceRequestType.USERNAME);
        request.getServletContext().setAttribute("user", user);
        
        return "redirect:/";
    }
	
	@GetMapping("/logout")
	public String logoutUser(HttpServletRequest request) {
		ServletContext context = request.getServletContext();
		Object o = context.getAttribute("user");
		
		if(o == null) {
			return "redirect:/";
		} 
		
		context.removeAttribute("user");	
		return "index";	
	}
}
