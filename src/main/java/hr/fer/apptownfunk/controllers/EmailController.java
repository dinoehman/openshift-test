package hr.fer.apptownfunk.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hr.fer.apptownfunk.models.Users;
import hr.fer.apptownfunk.services.UserService;
import hr.fer.apptownfunk.services.UserServiceRequestType;

@Controller
public class EmailController {
	
	@Autowired
	UserService userService;

	@GetMapping("/validateEmail")
	public String validateEmail(@RequestParam String t) {
		Users user = userService.getUser(t, UserServiceRequestType.VALIDATION_TOKEN);
		
		if(user == null) {
			return "successful";
		}
		
		user.setEmailVerified(true);
		userService.updateUser(user);
		
		return "email";
		
	}

}
