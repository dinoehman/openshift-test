package hr.fer.apptownfunk.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import hr.fer.apptownfunk.models.ExhibitDetails;
import hr.fer.apptownfunk.models.Exhibits;
import hr.fer.apptownfunk.models.filebucket.FileBucket;
import hr.fer.apptownfunk.models.filebucket.MultiFileBucket;
import hr.fer.apptownfunk.services.ExhibitDetailsService;
import hr.fer.apptownfunk.services.ExhibitsService;

@Controller
public class ExhibitsController {

	@Autowired
	private ExhibitsService exhibitsService;

	@Autowired
	private ExhibitDetailsService exhibitsDetailsService;

	@Autowired
	private MultiFileBucket detailsModel;

	@GetMapping("/exhibitUpload")
	public String showExhibitUploadPage(ModelMap model) {
		model.addAttribute("fileBucket", detailsModel);
		return "exhibitUpload";
	}

	@PostMapping(value = "/exhibits", headers = ("content-type=multipart/*"), consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addNewExhibit(ModelMap map, @RequestParam String name, @RequestParam String description,
			MultiFileBucket files) {
		String uploadFolder = "src/main/resources/static/";
		List<FileBucket> filesList = files.getFiles();
		
		try {
			FileCopyUtils.copy(filesList.get(0).getFile().getBytes(),
					new File(uploadFolder + "image/" + filesList.get(0).getFile().getOriginalFilename()));
			FileCopyUtils.copy(filesList.get(1).getFile().getBytes(),
					new File(uploadFolder + "audio/" + filesList.get(1).getFile().getOriginalFilename()));
		} catch (IOException e) {
		}

		Exhibits exhibit = new Exhibits(0, description, name, filesList.get(1).getFile().getOriginalFilename(),
				filesList.get(0).getFile().getOriginalFilename());
		exhibitsService.addExhibit(exhibit);

		return getAllExhibits(map);

	}

	@PutMapping("/exhibits/{id}")
	public void updateExhibit(@RequestBody Exhibits exhibitObjects) {
		exhibitsService.updateExhibit(exhibitObjects);
	}

	@DeleteMapping("/exhibits/{id}")
	public void deleteExhibit(ModelMap map, @PathVariable Integer id) {
		String uploadFolder = "src/main/resources/static/";
		Exhibits exhibit = exhibitsService.getExhibitById(id);
		String imagePath = exhibit.getImage_path();
		String audioPath = exhibit.getAudio_path();
		
		try {
			File image = new File(uploadFolder + "image/" + imagePath);
			File audio = new File(uploadFolder + "audio/" + audioPath);
			
			if (!image.delete()) {
				System.out.println("Došlo je do pogreške 1");
			}
			if (!audio.delete()) {
				System.out.println("Došlo je do pogreške 2");
			}
		} catch (Exception e) {
		}

		// Obriši exhibit
		exhibitsService.deleteExhibit(id);

		// Obriši sve detalje vezane uz exhibit
		exhibitsDetailsService.getAllExhibitDetailsExhibitId(id).forEach(detail -> {
			deleteExhibitDetails(detail.getDetailId());
		});
	}

	private void deleteExhibitDetails(int id) {
		String uploadFolder = "src/main/resources/static/";
		ExhibitDetails detail = exhibitsDetailsService.getExhibitDetail(id);
		String imagePath = detail.getImage_path();
		String audioPath = detail.getAudio_path();
		
		try {
			File image = new File(uploadFolder + "image/" + imagePath);
			File audio = new File(uploadFolder + "audio/" + audioPath);
			if (!image.delete()) {
				System.out.println("Došlo je do pogreške 1");
			}
			if (!audio.delete()) {
				System.out.println("Došlo je do pogreške 2");
			}
		} catch (Exception e) {
		}
		
		exhibitsDetailsService.deleteExhibitDetail(id);
	}

	@GetMapping("/exhibits")
	public String getAllExhibits(ModelMap model) {
		model.addAttribute("exhibits", exhibitsService.getAllExhibits());
		return "allExhibits";
	}

	// @GetMapping("/exhibits/{id}")
	// public Exhibits getExhibitById(@PathVariable int id) {
	// return exhibitsService.getExhibitById(id);
	// }

	@GetMapping("/exhibits/{id}")
	public ModelAndView getExhibitById(ModelMap model, @PathVariable Integer id) {
		ModelAndView modelAndview = new ModelAndView("exhibit");
		model.addAttribute("exhibitDetails", exhibitsDetailsService.getAllExhibitDetailsExhibitId(id));
		model.addAttribute("exhibit", exhibitsService.getExhibitById(id));
		
		return modelAndview;
	}

	@GetMapping("/addExhibits/{id}")
	public ModelAndView showAddExhibits(ModelMap model, @PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("addExhibits");
		List<Exhibits> exhibits = exhibitsService.getAllExhibits();
		
		exhibits.removeAll(exhibitsService.getExhibitsByExhibitionId(id));
		model.addAttribute("exhibits", exhibits);
		
		return modelAndView;
	}

	@PostMapping("/addExhibits/{id}")
	public ModelAndView addExhibits(ModelMap model, @PathVariable Integer id,
			@RequestParam("selected") int[] exhibitId) {
		exhibitsService.addExhibitsToExhibition(id, exhibitId);
		return showAddExhibits(model, id);
	}

	@GetMapping("/removeExhibits/{id}")
	public ModelAndView showRemoveExhibits(ModelMap model, @PathVariable Integer id) {
		ModelAndView modelAndView = new ModelAndView("removeExhibits");
		model.addAttribute("exhibits", exhibitsService.getExhibitsByExhibitionId(id));
		model.addAttribute("exhibitionId", id);
		
		return modelAndView;
	}

	@PostMapping("/removeExhibits/{id}")
	public ModelAndView removeExhibits(ModelMap model, @PathVariable Integer id,
			@RequestParam("selected") int[] exhibitId) {
		exhibitsService.removeExhibitsFromExhibition(id, exhibitId);
		return showRemoveExhibits(model, id);
	}

}
