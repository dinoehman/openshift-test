﻿package hr.fer.apptownfunk.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import hr.fer.apptownfunk.models.ExhibitDetails;
import hr.fer.apptownfunk.models.filebucket.FileBucket;
import hr.fer.apptownfunk.models.filebucket.MultiFileBucket;
import hr.fer.apptownfunk.services.ExhibitDetailsService;

@Controller
public class ExhibitDetailsController {

	@Autowired
	private ExhibitDetailsService exhibitDetailsService;

	@Autowired
	private MultiFileBucket detailsModel;

	@GetMapping("/detailsUpload/{id}")
	public String showDetailsUploadPage(@PathVariable Integer id, ModelMap model) {
		model.addAttribute("fileBucket", detailsModel);
		model.addAttribute("exhibitId", id);
		return "detailsUpload";
	}

	@PostMapping(value = "/exhibitDetails/{id}", headers = ("content-type=multipart/*"), consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addNewExhibitDetail(ModelMap map, @RequestParam String name, @RequestParam String description,
			MultiFileBucket files, @PathVariable Integer id) {
		String uploadFolder = "../src/main/resources/static/";
		List<FileBucket> filesList = files.getFiles();

		// Spremanje datoteka na disk
		try {
			FileCopyUtils.copy(filesList.get(0).getFile().getBytes(),
					new File(uploadFolder + "image/" + filesList.get(0).getFile().getOriginalFilename()));
			FileCopyUtils.copy(filesList.get(1).getFile().getBytes(),
					new File(uploadFolder + "audio/" + filesList.get(1).getFile().getOriginalFilename()));
		} catch (IOException e) {
		}

		// Spremanje u bazu
		ExhibitDetails exhibitDetail = new ExhibitDetails(0, id, description,
				filesList.get(1).getFile().getOriginalFilename(), filesList.get(0).getFile().getOriginalFilename(),
				name);
		exhibitDetailsService.addExhibitDetail(exhibitDetail);

		return getAllExhibitDetails(map);

	}

	@PutMapping("/exhibitDetails/{id}")
	public void updateExhibitDetail(@RequestBody ExhibitDetails exhibit) {
		exhibitDetailsService.updateExhibitDetail(exhibit);
	}

	@DeleteMapping("/exhibitDetails/{id}")
	public void deleteExhibitDetail(ModelMap map, @PathVariable Integer id) {
		String uploadFolder = "src/main/resources/static/";
		ExhibitDetails detail = exhibitDetailsService.getExhibitDetail(id);
		String imagePath = detail.getImage_path();
		String audioPath = detail.getAudio_path();
		
		try {
			File image = new File(uploadFolder + "image/" + imagePath);
			File audio = new File(uploadFolder + "audio/" + audioPath);
			
			if (!image.delete()) {
				System.out.println("Došlo je do pogreške 1");
			}
			if (!audio.delete()) {
				System.out.println("Došlo je do pogreške 2");
			}
		} catch (Exception e) {
		}
		
		exhibitDetailsService.deleteExhibitDetail(id);
	}

	// Mozemo maknuti poslije
	@GetMapping("/exhibitDetails")
	public String getAllExhibitDetails(ModelMap model) {
		model.addAttribute("details", exhibitDetailsService.getAllExhibitDetails());
		return "allExhibitDetails";
	}

	// @GetMapping("/exhibitDetails/{id}")
	// public ExhibitDetails getExhibitDetailById(ModelMap model, @PathVariable
	// Integer id) {
	// return exhibitDetailsService.getExhibitDetail(id);
	// }

	@GetMapping("/exhibitDetails/{id}")
	public ModelAndView getExhibitDetailById(ModelMap model, @PathVariable Integer id) {
		ModelAndView modelAndview = new ModelAndView("exhibitDetail");
		model.addAttribute("detail", exhibitDetailsService.getExhibitDetail(id));
		return modelAndview;
	}
}
