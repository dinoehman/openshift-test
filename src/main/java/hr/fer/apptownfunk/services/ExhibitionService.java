package hr.fer.apptownfunk.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.fer.apptownfunk.models.Exhibitions;
import hr.fer.apptownfunk.repository.ExhibitionsRepository;

@Service
public class ExhibitionService {

	@Autowired
	private ExhibitionsRepository exhibitionsRepository;

	public List<Exhibitions> getAllExhibitions() {
		List<Exhibitions> exhibitions = new ArrayList<>();
		exhibitionsRepository.findAll().forEach(exhibitions::add);

		return exhibitions;
	}

	public Exhibitions getExhibition(Integer id) {
		return exhibitionsRepository.findOne(id);
	}

	public void addExhibition(Exhibitions exhibit) {
		exhibitionsRepository.save(exhibit);
	}

	public void updateExhibition(Exhibitions exhibit) {
		exhibitionsRepository.save(exhibit);
	}

	public void deleteExhibition(Integer id) {
		exhibitionsRepository.delete(id);
	}
}
