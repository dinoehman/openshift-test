ALTER TABLE exhibitions ADD CONSTRAINT chk_dateEnd CHECK (date_End >= date_Begin);
INSERT INTO exhibitions(name_Of_Exhibition, date_Begin, date_End, description) VALUES ('picasso', '2017-2-18', '2017-2-28', 'best of young artist named picasso');
INSERT INTO exhibitions(name_Of_Exhibition, date_Begin, date_End, description) VALUES ('fossils', '2017-2-19', '2017-2-28', 'come look at some ancient fossils');
INSERT INTO exhibits(name, description) VALUES ('mona lisa', 'half-length portrait painting by the Italian Renaissance artist Leonardo da Vinci that has been described as "the best known, the most visited, the most written about, the most sung about, the most parodied work of art in the world".');
INSERT INTO exhibits(name, description) VALUES ('The Virgin and Child with St. Anne', 'The Virgin and Child with Saint Anne is an oil painting by Leonardo da Vinci depicting St Anne, her daughter the Virgin Mary and the infant Jesus. Christ is shown grappling with a sacrificial lamb symbolizing his Passion as the Virgin tries to restrain him. The painting was commissioned as the high altarpiece for the Church of Santissima Annunziata in Florence and its theme had long preoccupied Leonardo.');
INSERT INTO users(first_name, last_name, username, password, email, role,email_verified, validation_token) VALUES ('Tom', 'Smith', 'tommy', '843ut9384hjf', 'tommyboy@yahoo.com', 'admin','true', '12321321412');
INSERT INTO users(first_name, last_name, username, password, email, role,email_verified, validation_token) VALUES ('Frank', 'Castle', 'Punisher', '843ut9384hjd', 'onebatch@yahoo.com', 'admin','true', '12321321423');
INSERT INTO users(first_name, last_name, username, password, email, role,email_verified, validation_token) VALUES ('Tony', 'Stark', 'IronMan', '843ut9384123', 'ironboy@yahoo.com', 'admin','true', '12321321434');
INSERT INTO users(first_name, last_name, username, password, email, role,email_verified, validation_token) VALUES ('Steve', 'Rogers', 'Cap', '843ut9384345', 'mericaboy@yahoo.com', 'registrated_user','true', '12321321534');
INSERT INTO users(first_name, last_name, username, password, email, role,email_verified, validation_token) VALUES ('Bruce', 'Banner', 'Hulky', '843ut9323456', 'HulkTheMan@yahoo.com', 'registrated_user','true', '12321321876');


